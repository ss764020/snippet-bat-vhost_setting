@ECHO off
REM ***************************************************************
REM * WSL Ubuntu/XAMPP Apache バーチャルホスト設定自動化バッチファイル
REM * @ Rev: 3.1.0
REM * @ author: y.ohta
REM ***************************************************************

SETLOCAL enabledelayedexpansion

REM 管理者権限で実行していない場合はエラー
openfiles > NUL 2>&1 
IF NOT %ERRORLEVEL% EQU 0 goto NotAdmin 

  REM 外部設定ファイルのパス定義
  SET ConfFile=%~dp0.ini\common.ini
  REM 外部設定ファイルの読み込み
  IF NOT EXIST %ConfFile% (
    ECHO [error] - "%ConfFile%" is not found
    PAUSE
  ) ELSE (
    REM 外部設定ファイルから変数の取り出し
    FOR /f "tokens=1,* delims==" %%a IN (%ConfFile%) DO (
      SET %%a=%%b
    )

    REM 外部設定ファイルのドキュメントルートディレクトリが正しいか
    SET IS_DIR=TRUE
    IF NOT EXIST !WinDocRoot! (
      ECHO [error] - "!WinDocRoot!" is not found
      SET IS_DIR=FALSE
    )
    IF NOT EXIST !WinSiteConfRoot! (
      ECHO [error] - "!WinSiteConfRoot!" is not found
      SET IS_DIR=FALSE
    )
    IF !IS_DIR! == FALSE (
      ECHO Please read "README.txt", and edit ".common.ini"
      PAUSE
    ) ELSE (
      REM 設定ドメインを入力要求
      SET /P DOMAIN_STR="ヴァーチャルホストに設定するドメイン名を入力してください: " %DOMAIN_STR%

      REM wwwありも登録するか否かを入力要求
      SET /P FLG_WWW="「wwwあり」でも登録しますか？(y/n):" %FLG_WWW%

      REM アラート出力
      ECHO hostファイルに「!DOMAIN_STR!」に関する情報を書き込みます

      REM hostファイルに情報書き込み
      ECHO 127.0.0.1                   !DOMAIN_STR! >> C:\Windows\System32\drivers\etc\hosts

      REM wwwありでも書き込み
      IF "!FLG_WWW!" == "y" (
        ECHO hostsファイルに wwwあり でも情報を書き込んでいます
        ECHO 127.0.0.1                   www.!DOMAIN_STR! >> C:\Windows\System32\drivers\etc\hosts
      )


      REM hostsファイルをメモ帳（管理者権限）で開く
      powershell -NoProfile -ExecutionPolicy unrestricted -Command "start notepad C:\Windows\System32\drivers\etc\hosts -verb runas"

      REM htdocsにディレクトリを作成、エクスプローラ表示
      mkdir !WinDocRoot!\!DOMAIN_STR!
      start !WinDocRoot!\!DOMAIN_STR!

      REM ヴァーチャルホストのconfファイル格納ディレクトリへ移動
      cd /d !WinSiteConfRoot!

      REM confファイルを生成
      ECHO !DOMAIN_STR!.confファイルを生成します
      ECHO !DOMAIN_STR!.confファイルに!DOMAIN_STR!に関する情報を書き込んでいます
      ECHO # !DOMAIN_STR! > !DOMAIN_STR!.conf
      ECHO ^<VirtualHost *:80^> >> !DOMAIN_STR!.conf
      ECHO ServerName !DOMAIN_STR! >> !DOMAIN_STR!.conf
      IF !TYPE! == wsl (
        REM WSL Ubuntu上のパスで設定
        ECHO DocumentRoot "/var/www/html/!DOMAIN_STR!" >> !DOMAIN_STR!.conf
      ) ELSE IF !TYPE! == wsl2 (
        REM WSL2 Ubuntu上のパスで設定
        ECHO DocumentRoot "/var/www/html/!DOMAIN_STR!" >> !DOMAIN_STR!.conf
      ) ELSE (
        REM XAMPP上のパスで設定
        ECHO DocumentRoot "!WinDocRoot!\!DOMAIN_STR!" >> !DOMAIN_STR!.conf
      )

      ECHO ^</VirtualHost^> >> !DOMAIN_STR!.conf

      REM wwwありでも書き込み
      IF "!FLG_WWW!" == "y" (
        ECHO !DOMAIN_STR!.confファイルに wwwあり でも情報を書き込んでいます
        ECHO # www.!DOMAIN_STR! >> !DOMAIN_STR!.conf
        ECHO ^<VirtualHost *:80^> >> !DOMAIN_STR!.conf
        ECHO ServerName www.!DOMAIN_STR! >> !DOMAIN_STR!.conf
        IF !TYPE! == wsl (
          REM WSL Ubuntu上のパスで設定
          ECHO DocumentRoot "/var/www/html/!DOMAIN_STR!" >> !DOMAIN_STR!.conf
        ) ELSE IF !TYPE! == wsl2 (
          REM WSL2 Ubuntu上のパスで設定
          ECHO DocumentRoot "/var/www/html/!DOMAIN_STR!" >> !DOMAIN_STR!.conf
        ) ELSE (
          ECHO DocumentRoot "!WinDocRoot!\!DOMAIN_STR!" >> !DOMAIN_STR!.conf
        )
        ECHO ^</VirtualHost^> >> !DOMAIN_STR!.conf
      )

      REM 生成したconfファイルを指定アプリで開く
      start !DOMAIN_STR!.conf

      REM WSL Ubuntu上のApache に登録
      IF !TYPE! == wsl (
        wsl sudo ln -s !WslDocRoot!/!DOMAIN_STR! /var/www/html/!DOMAIN_STR!
        wsl sudo ln -s !WslSiteConfRoot!/!DOMAIN_STR!.conf /etc/apache2/sites-available/!DOMAIN_STR!.conf
        wsl cd /etc/apache2/sites-available/;sudo a2ensite !DOMAIN_STR!.conf
      ) ELSE IF !TYPE! == wsl2 (
        wsl sudo ln -s !WslSiteConfRoot!/!DOMAIN_STR!.conf /etc/apache2/sites-available/!DOMAIN_STR!.conf
        wsl cd /etc/apache2/sites-available/;sudo a2ensite !DOMAIN_STR!.conf
      )
    )
  )
  
goto End

:NotAdmin
  ECHO.
  ECHO [ERROR] NOT ADMIN
  ECHO.
  ECHO - Please run this file with "Administor" permission
  ECHO.
  PAUSE
:End