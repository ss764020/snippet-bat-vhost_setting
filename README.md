﻿/**  
* @ title:  WSL Ubuntu/XAMPP Apache バーチャルホスト設定自動化バッチファイル  
* @ author: y.ohta（https://auto-you-skit.com）  
* @ create: 2016.02.04  
* @ update: 2021.10.01  
* @ rev.:   4.0.0  
**/  
  
## 【0. 概要】  

**(0-1)**  
本バッチファイルの実行によって、  
WSL Ubuntu上もしくはXampp上のApache Virtualhostの設定(ドキュメントルートディレクトリの作成やhostsファイルの書き換えも)ができる  

**(0-2)**  
www ありサブドメインも同時登録可能  

**(0-3)**  
hostsファイルの書き換えをするため、必ず管理者権限で実行すること（!重要!）  
※ これをしていないと、エラー終了する  

**(0-4)**  
事前に下記(2-1),(2-2)のとおり、ドキュメントルートディレクトリに合わせて .ini/common.iniの内容を書き換えること  

**(0-5)**  
事前に下記(2-3)のとおりApacheの設定をしておくこと  



## 【1. 内容】  

**(1-1)**  
/vhost_setting_bat.bat  
 … 本体ファイル  


**(1-2)**  
/.ini/common.ini.sample  
 … 共通設定ファイルサンプル  



## 【2. 事前準備】  

**(2-1)**  
/.ini/common.ini.sample  
を複製し  
/.ini/common.ini  
として保存。  


**(2-2)**  
/.ini/common.ini  
をテキストエディタで開き、  
Windows上およびWSL上のドキュメントルートのパスを適宜書き換える  


**(2-3)**  
（Xamppの場合）以下の通りApacheの設定を変更する  

**①**  
（※ Xamppインストールディレクトリを `C:\xampp` と仮定して説明している。  
別ディレクトリにインストールした場合は適宜読み替えること。）  

**(i)**  
（※ Xamppインストールディレクトリが `C:\xampp` と仮定した場合）    
C:\xampp\apache\conf\extra\vhosts  
を作成しておく  

**(ii)**  
（※ Xamppインストールディレクトリが `C:\xampp` と仮定した場合）  
C:\xampp\apache\conf\httpd.conf  
をテキストエディタで開き、  

**(516行目付近)**

```
Include conf/extra/httpd-vhosts.conf
```

の下に

```
Include conf/extra/vhosts/*.conf
```

と追記する  


**②**  
/.ini/common.ini  

の  

`WinDocRoot`  

の値を`xampp`に変更する  





## 【3. 使用方法】  

**(3-1)**  
本プログラム（/vhost_setting_bat.bat）を管理者権限で実行する  


**(3-2)**  
コマンドプロンプトが立ち上がりホスト名の入力を求められるので、出力にしたがって入力するとヴァーチャルホストの設定ができる  

※ このとき、作業しやすいように下記のように動作する  

1. 編集されたhostsファイルがメモ帳で開かれる  
1. 作成されたドキュメントルートディレクトリがエクスプローラで表示される  
1. 作成されたヴァーチャルホスト設定ファイルが指定アプリケーションで開かれる  


**(3-3)**  
設定完了後、Xampp Apache（※もしくはWSL Ubuntu の Apache）を（再）起動することで設定が反映される  

